# [angular](https://www.npmjs.com/package/angular)

AngularJS - HTML enhanced for web apps! https://angularjs.org

(Unofficial Demo and HowTo)

* Wikipedia: [AngularJS](https://en.wikipedia.org/wiki/AngularJS)
* [*[Angular + Symfony] JWT Authentication*
  ](https://medium.com/@liujijieseason/angular-symfony-jwt-authentication-5fd39da2a1ac)
  2020-07 Jijie Liu
* [*Tutorial: Build Your First CRUD App with Symfony and Angular*
  ](https://developer.okta.com/blog/2018/08/14/php-crud-app-symfony-angular)
  2018-08

# Popularity
 * https://insights.stackoverflow.com/survey/2020#technology-web-frameworks
